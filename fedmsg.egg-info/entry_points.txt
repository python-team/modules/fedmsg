[moksha.producer]


[fedmsg.meta]
announce = fedmsg.meta.announce:AnnounceProcessor
logger = fedmsg.meta.logger:LoggerProcessor

[moksha.consumer]
fedmsg-dummy = fedmsg.consumers.dummy:DummyConsumer
fedmsg-relay = fedmsg.consumers.relay:RelayConsumer
fedmsg-ircbot = fedmsg.consumers.ircbot:IRCBotConsumer
fedmsg-gateway = fedmsg.consumers.gateway:GatewayConsumer

[console_scripts]
fedmsg-tail = fedmsg.commands.tail:tail
fedmsg-dg-replay = fedmsg.commands.replay:replay
fedmsg-announce = fedmsg.commands.announce:announce
fedmsg-irc = fedmsg.commands.ircbot:ircbot
fedmsg-gateway = fedmsg.commands.gateway:gateway
fedmsg-relay = fedmsg.commands.relay:relay
fedmsg-hub = fedmsg.commands.hub:hub
fedmsg-collectd = fedmsg.commands.collectd:collectd
fedmsg-trigger = fedmsg.commands.trigger:trigger
fedmsg-logger = fedmsg.commands.logger:logger

