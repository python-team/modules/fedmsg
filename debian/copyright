Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fedmsg
Source: http://fedmsg.com/

Files: *
Copyright: 2008-2014 Red Hat, Inc
License: LGPL-2.1+

Files: fedmsg/replay/* fedmsg/tests/test_replay.py
Copyright: 2013 Simon Chopin <chopin.simon@gmail.com>
License: LGPL-2.1+

Files: fedmsg/commands/config.py
Copyright: 2012-2014 Red Hat, Inc,
           2014 Nicolas Dandrimont <olasd@debian.org>
License: LGPL-2.1+

Files: debian/*
Copyright: 2013 Simon Chopin <chopin.simon@gmail.com>,
           2013-2014 Nicolas Dandrimont <olasd@debian.org>
License: LGPL-2.1+

Files: fedmsg/tests/test_hub.py fedmsg/tests/test_threads.py
Copyright: 2008-2010 Red Hat, Inc
License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, a copy of the Apache 2.0 license can be found at
 `/usr/share/common-licenses/Apache-2.0`

Files: fedmsg/tests/test_certs/pkitool fedmsg/tests/test_certs/tmp/pkitool
Copyright: 2002-2010 OpenVPN Technologies, Inc
License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program (see the file COPYING included with this
 distribution); if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, a copy of the GNU General Public License can be found at
 `/usr/share/common-licenses/GPL-2`

License: LGPL-2.1+
 fedmsg is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 fedmsg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with fedmsg; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, a copy of the license can be found at
 `/usr/share/common-licenses/LGPL-2.1`.
