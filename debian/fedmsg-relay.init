#! /bin/sh
### BEGIN INIT INFO
# Provides:          fedmsg-relay
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: fedmsg relay daemon
# Description:       Relay daemon for the fedmsg bus
### END INIT INFO

# Author: Nicolas Dandrimont <olasd@debian.org>

PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="relay daemon for the fedmsg bus"
NAME=fedmsg-relay
DAEMON=/usr/bin/$NAME
PIDFILE=/var/run/fedmsg/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
USER=fedmsg

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.2-14) to ensure that this file is present
# and status_of_proc is working.
. /lib/lsb/init-functions

do_start()
{
        if ! [ -d /var/run/fedmsg ]; then
                mkdir /var/run/fedmsg
                chown $USER /var/run/fedmsg
        fi

	start-stop-daemon --start --quiet --user $USER --pidfile $PIDFILE --exec $DAEMON --test > /dev/null \
		|| return 1
	start-stop-daemon --start --quiet --user $USER --pidfile $PIDFILE --chuid $USER --background --startas /bin/sh -- -c "( $DAEMON 2>&1 & echo \$! > $PIDFILE ) | logger -p daemon.info -t $NAME &" \
		|| return 2
}

do_stop()
{
	start-stop-daemon --stop --retry=TERM/30/KILL/5 --quiet --user $USER --pidfile $PIDFILE
	RETVAL="$?"
	[ "$RETVAL" = 2 ] && return 2
	start-stop-daemon --stop --retry=0/30/KILL/5 --oknodo --quiet --user $USER --pidfile $PIDFILE
	[ "$?" = 2 ] && return 2
	rm -f $PIDFILE
	return "$RETVAL"
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
	do_start
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
	status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
	;;
  restart|force-reload)
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0|1)
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
		# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
	exit 3
	;;
esac

:
